<?php

$title = "Page Not Found | John S Macdonald Builders NZ";
$description = "Sorry, we can't find the page you are looking for. Please try one of the links in the menu.";

include_once 'header.php';

?>
    <div class="container main">
        <div class="row">
            <div class="col-lg-12" role="main">

                <h1>Page not found</h1>
                <p>Sorry, we can't find the page you are looking for. This may be because it no longer exists or the address was misspelled. Use the menu above to navigate to an existing page instead.</p>


            </div>
        </div>
        <!-- /.row -->

        <?php include_once 'footer.php'; ?>