<?php session_start();

$title = "Contact Us | John S Macdonald Builders NZ";
$description = "You can contact us using the contact form, via email or call right from the website. Our premises are located in Matangi, Waikato New Zealand.";

include_once 'header.php';

?>
    <div class="container main">
        <div class="row">
            <div class="col-lg-6" role="main">

                <h1>Contact Us</h1>
                <h2>Address</h2>
                <p> <span class="bold">Postal Address</span>
                    <br/> John S Macdonald Builders Ltd
                    <br/> PO Box 110
                    <br/> Matangi 3260</p>

                <p> <span class="bold">Physical Address</span>
                    <br/> 452 Tauwhare Road</br>
                    RD 4</br>
                    Matangi 3284</p>


                <h2>Contact Details</h2>
                <p><span class="bold">Ph </span><a href="tel:+6478558855">07 855 8855</a>
                    <br />
                    <span class="bold">A/H</span><a href="tel:+64274863653"> 027 4863 653</a>
                </p>
                <p> <a href="mailto:john@jsmb.co.nz" target="_top"><span class="bold">Email Us </span></a>
                </p>

                <p> <span class="bold">Office Hours</span></p>
                <ul class="hours-container">
                    <li>Monday-Friday <span class="opening-hours">7:30am - 5:00pm</span></li>
                    <li>Saturday-Sunday <span class="opening-hours">Closed</span></li>
                </ul>
                <a href="https://plus.google.com/+JohnSMacdonaldBuildersLtdHamilton/about" target="_blank" class="float-link"><img src="_img/google-plus.svg" alt="Google+" class="social"></a>
            </div>


            <div class="col-lg-6">
                <h2>Contact Form</h2>
                <?php if(isset($_SESSION['alertMessage'])) { 
                        // Show the alert message 
                        echo $_SESSION['alertMessage']; 
                        // Remove the message so its not there after a refresh
                        unset($_SESSION['alertMessage']); 
                    }

                    if(isset($_SESSION['postData'])) {
                        $postData = $_SESSION['postData'];
                    } else {
                        $postData = [];
                    }
                ?>


                    <form action="form.php" method="post" enctype="multipart/form-data" data-parsley-validate>
                        <p>All fields marked with asterisk (<span class="required">*</span>) are required</p>


                        <div class="form-group">

                            <label for="name">Name <span class="required">*</span>
                            </label>

                            <input type="text" class="form-control" size="30" minlength="3" maxlength="50" name="name" id="name" placeholder="Your Name" data-parsley-trigger="change" required value="<?php if (isset($postData['name'])) echo htmlspecialchars($postData['name']);?>" />

                        </div>

                        <div class="form-group">
                            <label for="email">Email <span class="required">*</span>
                            </label>

                            <input type="email" class="form-control" size="30" maxlength="50" name="email" id="email" placeholder="you@yourmail.com" data-parsley-trigger="change" required value="<?php if (isset($postData['email'])) echo htmlspecialchars($postData['email']);?>">
                            <small id="emailHelp" class="form-text text-muted">Your email will not be shared with third parties.</small>
                        </div>


                        <div class="form-group">
                            <label for="phone">Phone <span class="required">*</span>
                            </label>

                            <input type="text" class="form-control" size="19" minlength="7" maxlength="19" name="phone" id="phone" placeholder="07 855 8855" pattern="^\+?[0-9 ]*$" data-parsley-trigger="change" required value="<?php if (isset($postData['phone'])) echo htmlspecialchars($postData['phone']);?>" />
                        </div>



                        <div class="form-group">
                            <label for="message">Message <span class="required">*</span>
                            </label>

                            <textarea class="form-control" name="message" rows="5" cols="29" minlength="10" maxlength="500" placeholder="Please write your message here. Min length 10 characters, max length 500 characters." data-parsley-trigger="keyup" data-parsley-validation-threshold="1" data-parsley-minlength-message="Please enter a message at least 10 characters long." required><?php if (isset($postData['message'])) echo htmlspecialchars($postData['message']);?></textarea>
                        </div>


                        <div class="form-group">
                            <label for="where">How did you hear about JSMB? <span class="required">*</span></label>

                            <?php
                            $selected = 0;
                            if (isset($postData['where'])) {
                                $where = $postData['where'];
                                if ($where === 'Google-search') {
                                    $selected = 1;
                                } else if ($where === 'Word-of-mouth') {
                                    $selected = 2;
                                } else if ($where === 'Article') {
                                    $selected = 3;
                                } else if ($where === 'Vehicles') {
                                    $selected = 4;
                                } else if ($where === 'Ads') {
                                    $selected = 5;
                                } else if ($where === 'Other') {
                                    $selected = 6;
                                }
                            }
                        ?>
                                <select name="where" size="1" class="form-control" required>
                                    <option value="Google-search" <?php if ($selected===1 ) echo 'selected';?>> Google Search
                                    </option>
                                    <option value="Word-of-mouth" <?php if ($selected===2 ) echo 'selected';?>> Recommendation
                                    </option>
                                    <option value="Article" <?php if ($selected===3 ) echo 'selected';?>> Newspaper or Magazine Article
                                    </option>
                                    <option value="Vehicles" <?php if ($selected===4 ) echo 'selected';?>> Vehicles
                                    </option>
                                    <option value="Ads" <?php if ($selected===5 ) echo 'selected';?>> Ads or Signs
                                    </option>
                                    <option value="Other" <?php if ($selected===6 ) echo 'selected';?>> Other
                                    </option>
                                </select>
                        </div>

                            <div class="g-recaptcha captcha" data-sitekey="6LdSfygTAAAAAEVtHZVoALiP7maB_rL8iG6bjOmm"></div>
                            <noscript>
                                <div style="width: 302px; height: 482px;">
                                    <div style="width: 302px; height: 482px; position: relative;">
                                        <div style="width: 302px; height: 422px; position: absolute;">
                                            <iframe src="https://www.google.com/recaptcha/api/fallback?k=6LcH7QgTAAAAAEgm3ueOHICRgIZsilCrz4nqgIJx" frameborder="0" scrolling="no" style="width: 302px; height:422px; border-style: none;">
                                            </iframe>
                                        </div>
                                        <div style="width: 302px; height: 60px; border-style: none;
                                  bottom: 10px; left: 0px; margin: 0px; padding: 0px; right: 25px;
                                  background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px; 
                                  position: absolute;">
                                            <textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" placeholder="Enter reCAPTCHA code here" style="width: 250px; height: 40px; border: 1px solid #c1c1c1;
                                     margin: 10px 25px; padding: 0px; resize: none;"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </noscript>

                        <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Submit">

                        <input type="reset" class="btn btn-secondary" value="Clear" onclick="clearInput()">
                    </form>
            </div>
            <!-- /.row -->
        </div>

        <div class="row">
            <div class="col-lg-12">

                <h2>Location</h2>

                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d100876.29882657136!2d175.3957758!3d-37.8044657!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6d6d1c7ffafc637b%3A0xa3c8fcd95f05738d!2s452+Tauwhare+Rd%2C+Tamahere+3284%2C+New+Zealand!5e0!3m2!1sen!2sau!4v1427415722220" frameborder="0" style="border:0" class="gmap"></iframe>

            </div>
            <!-- /.row -->
        </div>

        <!--

                <img src="_photos/cars-crop.jpg" alt="JSMB Vehicles" class="img-responsive img-center">
-->



        <?php include_once 'footer.php'; ?>