var $contactBar = $(".contact-bar");
var $navbarHeader = $(".navbar-header");
var $logo = $(".navbar-brand > img");
var $galleryButton = $(".image-container > .btn");
var $galleryImage = $(".gallery-preview-img");


/* Hide contact bar on scroll and show again when user scrolls to top */

function hideShowContactBar() {
    if ($(window).scrollTop() > 0) {
        $contactBar.hide();
    } else {
        $contactBar.show();
    }
}

/* Resize menu on scroll */

function resizeMenu() {
    if ($(window).scrollTop() > 0) {
        $logo.css("max-height", "50px");
    } else {
        $logo.css("max-height", "70px");
    }
}

/* Only call functions on desktop screen sizes */
/* Combines both making nav and logo smaller and removing contact bar */
function changeHeightFunction() {
    if (window.innerWidth > 768) {
        hideShowContactBar();
        resizeMenu();
    }
    
    else {
        $logo.css("max-height", "70px");
    }
}

/* call on scroll */
$(window).scroll(function() {
    changeHeightFunction();
})

/* call when page loads (eg if user has scrolled halfway down and refreshes) */
window.onload = function() {
    changeHeightFunction();
}

/* call when window resizes (eg from mobile to desktop) */
$(window).resize(function() {
    changeHeightFunction();
})


/* Gallery image function on homepage*/
/* Mouse enter event for button */
$galleryButton.mouseenter(function() {
    $galleryImage.css("filter", "opacity(100%)");

});

/* hover events for gallery image (mouseenter and mouseleave combined) */
$galleryImage.hover(function() {
        $galleryImage.css("filter", "opacity(100%)");
    },
    function() {
        $galleryImage.css("filter", "opacity(50%)");
    });