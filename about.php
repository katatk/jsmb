<?php

$title = "About | John S Macdonald Builders NZ";
$description = "The team at John S Macdonald Builders (JSMB) are small but we are highly experienced. John Macdonald has over 25 years experience in the building industry and established JSMB in Hamilton in 1996.";

include_once 'header.php';

?>
    <div class="container main">
        <div class="row">
            <div class="col-lg-12" role="main">

                <h1>About JSMB</h1>
                <h2>Meet The Team</h2>
                <img src="_photos/team-photo.jpg" alt="JSMB Team" class="img-responsive img-center">
                <p>John S Macdonald Builders Ltd was established in 1996, we've been building for almost 20 years which makes us one of the most experienced builders in the Waikato. Our team are highly skilled, and we thrive on the challenge of constructing a range of commercial and residential buildings, big and small.</p>
                <p>John's staff have been carefully chosen not just for their professionalism, but for having the depth of practical knowledge to solve problems collaboratively and creatively. We are passionate, friendly and committed to doing the best possible job to achieve your project goals from start to finish.
                </p>

                <h2>Meet John</h2>
                <img src="_photos/john-sm.jpg" class="john img-responsive" alt="John Macdonald">
                <p> John's building career began over 35 years ago with an apprenticeship in Taranaki, while his specialist interest in masonry-construction developed through nearly a decade of building experience in the UK. On returning to New Zealand in 1996, he established JSMB which has since gained an impressive reputation with a portfolio of award-winning buildings. John has been working alongside his team to be one of the best and most reputable building companies ever since.</p>
                <div class="masterlogo-container">
                    <a href="http://www.masterbuilder.org.nz/" target="_blank"><img src="_img/MB-Logo-Lg.png" class="masterlogo" alt="Master Builders Logo"></a>

                </div>
                <p>John is currently the National President of the New Zealand Registered Master Builders Association, having served on both local and national executives for the past 12 years. John's passion is to help create a construction industry which places the greatest emphasis on workmanship of the highest quality, a principle which also forms the cornerstone of JSMB.
                </p>
                <p>For more information on Master Builders, visit the website at: <a href="http://www.masterbuilder.org.nz/" target="_blank">masterbuilder.org.nz</a>
                </p>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">

                <h2>Our Senior Staff</h2>
                <ul class="staff-photos">
                    <li>
                        <figure><img src="_photos/Wendy-Jackson-Office-Manager-sq.jpg" alt="Wendy Jackson, Office Manager" class="staff-img img-responsive">
                            <figcaption>Wendy Jackson, Office Manager</figcaption>
                        </figure>
                    </li>
                    <li>
                        <figure><img src="_photos/Darryn-Jackson-Quantity-Surveyor-sq.jpg" alt="Darryn Jackson, Quantity Surveyor" class="staff-img img-responsive">
                            <figcaption>Darryn Jackson, Quantity Surveyor</figcaption>
                        </figure>
                    </li>

                    <li>
                        <figure><img src="_photos/Ken-Hobern-Senior-Site-Supervisor-sq.jpg" alt="Ken Hobern, Senior Site Supervisor" class="staff-img img-responsive">
                            <figcaption>Ken Hobern, Senior Site Supervisor</figcaption>
                        </figure>
                    </li>
                    <li>
                        <figure><img src="_photos/Gary-Jackson-Site-Supervisor-sq.jpg" alt="Gary Jackson, Site Supervisor" class="staff-img img-responsive">
                            <figcaption>Gary Jackson, Site Supervisor</figcaption>
                        </figure>
                    </li>

                    <li>
                        <figure><img src="_photos/Luke-Jones-Site-Supervisor-sq.jpg" alt="Luke Jones, Site Supervisor" class="staff-img img-responsive">
                            <figcaption>Luke Jones, Site Supervisor</figcaption>
                        </figure>
                    </li>
                    <li>
                        <figure><img src="_photos/Dean-Selwyn-site-supervisor-sq.jpg" alt="Dean Selwyn, Site Supervisor" class="staff-img img-responsive">
                            <figcaption>Dean Selwyn, Site Supervisor</figcaption>
                        </figure>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <h2>Our Building Process</h2>

            </div>

            <div class="col-sm-12 col-lg-6 center">
                <img src="_img/icon-blueprint.png" class="process-icons">
                <h3>1) Design &amp; Plan</h3>
                <p>If you already have a designer, great! If not, we can recommend one. We will work with the designer to ensure your plan is within your budget. We can help you decide on materials and offer suitable alternatives if required.
                </p>

            </div>
            <div class="col-sm-12 col-lg-6 center">

                <img src="_img/icon-billing.png" class="process-icons">
                <h3>2) Cost Estimate</h3>
                <p>We provide you with a budget estimate. If this exceeds your budget, we can work with the designer to tweak your plan. We want you to have a realistic expectation for your project with no hidden suprises.</p>
            </div>

            <div class="col-sm-12 col-lg-6 center">
                <img src="_img/icon-consent.png" class="process-icons">
                <h3>3) Get Consent</h3>
                <p>Once you are happy with your quote from us, consent needs to be obtained. After the quote is accepted, your working drawings are submitted to local council for consent. We can take care of this process for you.</p>
            </div>
            <div class="col-sm-12 col-lg-6 center">
                <img src="_img/icon-build.png" class="process-icons">
                <h3>4) Let's Build</h3>
                <p>We're with you all the way. Regular meetings make sure everything is on track so that can you make informed decisions. We want you to enjoy the building process and we'll do everything we can to ensure your project goes smoothly.
                </p>
            </div>
       </div>
        <!-- /.row -->


        <hr>
        <div class="row">
            <div class="col-lg-12">
                <p><b>For more detailed information on the building process, we recommend you download:</b>
                    <br/>
                    <i class="fa fa-file-pdf-o"></i>
                    <a href="files/Building%20Amendment%20Act%202013%20Consumers%20Booklet.pdf" download="Building Act Consumer Guide.pdf">Building Act Consumer Guide.pdf</a> (4.30 MB)
                    <br/>
                    <i class="fa fa-globe"></i> <span style="font-style: italic">Source: <a href="http://www.mbie.govt.nz" target="_blank">www.mbie.govt.nz</a></span>
                </p>

            </div>

        </div>
        <!-- /.row -->

        <?php include_once 'footer.php'; ?>
