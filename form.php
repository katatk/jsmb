<?php session_start();

    function validateCaptcha($recaptchaResponse) {
        // Send our secret and the users reCAPTCHA resopnse  
        $recaptchaSecret ='6LdSfygTAAAAAHAWEpK5UGOAihFbPUVXRmxombH4';
        $recaptchaUrl = "https://www.google.com/recaptcha/api/siteverify";
        $data = [
            'secret' => $recaptchaSecret,
            'response' => $recaptchaResponse
        ];
        $options = [
            'http' => [
                'header' =>'Content-type: application/x-www-form-urlencoded',
                'method' => 'POST',
                'content' => http_build_query($data)
            ]
        ];

        // Run the request
        $context = stream_context_create($options);
        $response = file_get_contents($recaptchaUrl, false, $context);
        $result = json_decode($response, true);

        return $result['success'];
    }

    $success = '<div class="success"><p>Your message has been sent, we will be in touch with you soon.</p></div>';
    $unknownError = '<div class="error"><p>Something went wrong, please try again.</p></div>';
    $empty = '<div class="error"><p>Please fill in all required fields.</p></div>';

    $completeForm = (isset($_POST['name']) &&
                     isset($_POST['email']) &&
                     isset($_POST['phone']) &&
                     isset($_POST['message']) &&
                     isset($_POST['where']) && 
                     isset($_POST['g-recaptcha-response'])
                    );

    if ($completeForm === true) {
        $name = trim($_POST['name']);
        $userEmail = trim($_POST['email']);
        $phone = trim($_POST['phone']);
        $phone = str_replace(' ', '', $phone);
        $where = trim($_POST['where']);
        $message = trim($_POST['message']);
        $recaptchaResponse = trim($_POST['g-recaptcha-response']);
    } else {
        $_SESSION['alertMessage'] = $empty;  
        $_SESSION['postData'] = $_POST;
        error_log("Incomplete form submitted:\n".var_export($_POST, true));
        header("Location: contact.php");
        die();
    }

    $errorMessage = '';

    // Validate the name
    $validName = false;
    if (strlen($name) >= 3) {
        $validName = true;
    } else {
        $errorMessage .= "<li>Name is too short</li>";
    }

    // Validate the email
    $validEmail = false;
    if (filter_var($userEmail, FILTER_VALIDATE_EMAIL)) {
        $validEmail = true;
    } else {
        $errorMessage .= "<li>Email address is invalid</li>";
    }

    // Validate the phone
    $validPhone = false;
    if (strlen($phone) >= 7 && strlen($phone) <= 15) {
        if (preg_match('/^\+?\d*$/', $phone)) {
            $validPhone = true;
        } else {
            $errorMessage .= "<li>Phone number is invalid</li>";
        }
    } else {
        $errorMessage .= "<li>Phone number is invalid</li>";
    }

    // Validate the message
    $validMessage = false;
    if (strlen($message) < 10) {
        $errorMessage .= "<li>Message is less than 10 characters</li>";
    } else if (strlen($message) > 500){
        $errorMessage .= "<li>Message is greater than 500 characters</li>";
    } else {
        $validMessage = true;
    }
        
    // Validate the reCAPTCHA
    $validCaptcha = validateCaptcha($recaptchaResponse);
    if (!$validCaptcha) {
        $errorMessage .= "<li>Captcha failed, please try again</li>";   
    }

    $whereValues = ["Google-search", "Word-of-mouth", "Article", "Vehicles", "Ads", "Other"];

    // Validate the where did you hear about JSMB
    $validWhere = false;
    if (in_array($where, $whereValues)) {
        $validWhere = true;
        $where = str_replace('-', ' ', $where);
    } else {
        $errorMessage .= "<li>Please let us know where you heard about JSMB</li>";
    }

    $validForm = $validName && $validEmail && $validPhone && $validMessage && $validWhere && $validCaptcha;

    if (isset($_POST['submit'])) {
        if ($validForm) {
           $headers = "From:  $userEmail\r\n" .
            "Reply-To: $userEmail\r\n";
            $to = 'john@jsmb.co.nz'; 
            $subject = 'Form Submission';
            $body = "This email was sent via the form on jsmb.co.nz.\n";
            $body .= "From: $name\n";
            $body .= "Email: $userEmail\n";
            $body .= "Ph: $phone\n";
            $body .= "How did you hear about JSMB?: $where\n\n";
            $body .= "Message:\n $message";

            $emailDataLog = "To: $to\nfrom: $userEmail\nsubject: $subject\nbody: $body";
            if (mail($to, $subject, $body, $headers)) {
                unset($_SESSION['postData']);
                $_SESSION['alertMessage'] = $success;

                $log = "Email successfully sent:\n$emailDataLog";
            } else {
                $_SESSION['alertMessage'] = $unknownError;
                $_SESSION['postData'] = $_POST;

                $log = "Email failed to send:\n$userEmail\n data:\n$emailDataLog\n\npost data:\n".var_export($_POST, true);
            }
        } else {
            $log = "Invaild form submitted:\n".var_export($_POST, true);
            $_SESSION['alertMessage'] = "<ul class='error'>$errorMessage</ul>";
            $_SESSION['postData'] = $_POST;
        }
    } else {
        $log = "Form.php accessed with submitted data:\n".var_export($_POST, true);
    }

    error_log($log."\n");

    header("Location: contact");
    die();
?>