<?php

$title = "Testimonials | John S Macdonald Builders NZ";
$description = "We have received many testimonials from our valued clients around Hamilton. We are very proud of our work and really appreciate your feedback.";

include_once 'header.php';

?>


    <div class="container main">
            <div class="row">
                <div class="col-lg-12" role="main">
                    <h1>Testimonials</h1>
                    <p>We really look after our clients and make sure they have the best possible experience when working with us. Don't just take our word for it, see what they have to say...</p>

                    <h2>Harrowfield Drive, Hamilton</h2>
                    <blockquote>
                        <p>John, as you know over the last eleven months, since the construction started, I have already expressed in various occasions my sincere admiration for your excellent professional standards and the high calibre of people you have in your team.</p>
                        <p>However, after what you have done today in this project (the modification of the stairs in the lounge and now the demolition of the concrete foundation by the fountain) I am really short of words to describe the good feelings I have about you personally and Ken Hobern. Your immediacy, your leading by example, your effective efforts for making your customer happy is really outstanding, especially knowing how tremendously busy you must be in this period.</p>
                        <p>I was amazed this morning (and in other occasions in this project) how quickly and graciously you can accommodate and satisfy requests of variations made at the very last minute, no matter how hard it is to implement them.</p>
                        <p>In essence I am trying to say: I think that by engaging the professional services of you and your team I have done the best thing in this project, as I have not only the benefit of the highest professional standard, but also I have met a number excellent persons with whom I will not doubt have a long friendly relationship well after the completion of the construction of my new house.
                        </p>
                        <p>Thanks!</p>
                        <p><span class="review-author">Jo</span>
                        </p>
                    </blockquote>

                    <h2>St Andrew’s Church, Hamilton</h2>
                    <blockquote>
                        <p>For a major redevelopment of the outdated and dilapidated St Andrew’s Church Centre into a modern Church and Community Centre, St Andrew’s awarded a contract to this company at the end of a competitive Tender process. We chose John Macdonald’s ﬁrm based on a combination of three factors: its quality reputation, pricing, and ability to complete in a timely fashion.</p>
                        <p>The total time indicated in January 2010 by the architects as the likely construction- period was seven months (starting in early April), and it is remarkable that this timeframe has been achieved because during the winter severe wet weather impacted on the programme, especially in relation to concreting and painting.</p>

                        <p>On behalf of the CRC and as the Principal’s Representatives nominated in the Building Contract, Wynne Dymock and Lance Kendrick have very much enjoyed working on a day-to-day basis with John and his Site Supervisor Chris Fowlie. They have paid considerable attention to detail and have skillfully solved a multiplicity of problems during the rebuilding, on occasions suggesting alternatives which saved us money and achieved a similar effect.</p>

                        <p>John and Chris and their team have been unfailingly helpful and have delivered high standards of workmanship. They and all the consultants and subcontractors should feel a great sense of pride in what has been achieved. Thirty-eight companies have been involved in this redevelopment, with 22 of them directly subcontracted by John Macdonald Builders Ltd.</p>

                        <p>We recommend John S Macdonald Builders Ltd, wholeheartedly and with gratitude.
                        </p>

                                <p><span class="review-author">Wynne Dymock, CRC Convenor</br>
                    Lance Kendrick, CRC Secretary</span></p>
                    </blockquote>

                    <h2>River Road, Hamilton</h2>
                    <blockquote>
                        <p>Dear John,</p>

                        <p>Now that we are well and truly ensconced in our new home, it is time to sit back and reﬂect over the past 35 months and in particular, the planning and building of our new home.</p>

                        <p>At the beginning it seemed like a nightmare as we became increasingly disillusioned with people like loss adjustors who kept saying they were there to ‘help us’! Our good friend Errol Pope helped to win that battle, but not knowing what to expect next, we were more than a little apprehensive about builders and the building process.</p>

                        <p>John, from the moment you first drove in ‘off the road’ as it were and began giving us advice, to the hammering in of the ﬁnal nail (actually there are one or two nails still needed!) we could not have been more at ease. Throughout the whole process you made us feel that our decisions and ideas really counted and were valued by you and your team.</p>

                        <p>We feel happy knowing that our house has been skillfully built to an extremely high standard by builders who take pride in striving for excellence. Your advice and commonsense approach to our project has always been greatly appreciated.</p>

                        <p>We would like you to pass on our very sincere thanks to all the people in your team for their expertise, their very good humour and their positive and helpful manner.</p>
                        <p>Ken has been an outstanding leader, always making excellent decisions and managing the whole project with superb skill and craftsmanship. Roger’s prowess with brick and trowel were a marvel to behold. Mike and Cameron were a great support team working tirelessly. Nathan similarly was first rate and together with Luke (nicest smile), Phiron (most cheerful), Blair (champion river swimmer), and Chris (superb landscape gardener), and Sue (calm collector of all details and telephone numbers), you all managed to transform our dreams into reality.</p>

                        <p>We would like to thank you so much for making what seemed at the start for us an insurmountable project, totally achievable and for the most part, an extremely enjoyable and satisfying experience. We are very proud of our new house and we will continue to sing the praises of John McDonald Builders as long as building houses are spoken about.</p>

                        <p>With very best wishes to you and your great team of builders,</p>
                        <p><span class="review-author">Graham and Heather Campbell</span></p>
                    </blockquote>

                    <h2>Puketaha</h2>
                    <blockquote>
                        <p>Having lived in the same house for 36 years we were novices in the ins and outs of building a house. We had heard some disturbing stories from those who experienced disappointments when they built, e.g. less than expected quality, over runs in costs, extended completion date and inability to get a builder to return when a problem arose. We didn't have to worry about any of those things.</p>

                        <p>John Macdonald went to great lengths to answer any questions we had and to do so in a manner we could understand. His confident and friendly manner put us to ease. We were supplied with an in-depth list of actual costs for our new home which made the process extremely transparent and kept us informed as each stage of the house progressed.</p>

                        <p>Friends ask us if we are enjoying the new house and we tell them we love it! Would we build again? Most definitely, providing we could get John to build it. We have a quality home built by a quality builder and we couldn't be happier!</p>

                        <p>Warm Regards,</p>
                        <p><span class="review-author">Mike Coad</span></p>
                    </blockquote>

                    <h2>Rotowaro, Huntly</h2>
                    <blockquote>
                        <p>John Macdonald built our new masonry home at Rotowaro. The house is a 4 bedroom design with a great view overlooking the North Waikato. From the time of turning the first sod to moving into our new home, we have thoroughly enjoyed the experience of working with John and his team.</p>

                        <p>The pricing system used by John enabled us to see where our money was being spent, and at the planning stage enabled us to make adjustments to ensure we could afford the final product. During the building process we met with John every 1 to 2 weeks to discuss any areas of uncertainty in the plan or process.</p>

                        <p>We made a couple of minor changes during that process, including moving a concrete pillar, and packing out a plastered wall to give us the precise window reveals needed for blinds we intend to purchase. John was totally supportive of these design changes. Throughout the process, John's sole focus was to give us the house we wanted. He was very easy to work with and we enjoyed our discussions with John.</p>

                        <p>We believe the house John has built us is constructed to a high standard. John employs his own block layer and plasterer. This enabled us to work closely on the plastered finish we were after for the house. John also installed a Matai floor into the kitchen, which he achieved by making a recess in the concrete floor, onto which he installed framing followed by the end matched T&#38;G timber. The floor looks fantastic, and the installation method has given us a floor that is very nice to walk on. This example is indicative of the time John spends on "getting it right".</p>

                        <p>We wouldn't hesitate to recommend John S Macdonald Builders to others.</p>
                        <p><span class="review-author">Mark and Robyn Harris</span>
                        </p>
                    </blockquote>

                    <h2>St James Park, Hamilton</h2>
                    <blockquote>
                        <p>When my wife and I decided to build our second home there was only one option for the method of construction, and that was a Firth Masonry Villa. After a few discussions we chose an experienced Masonry Villa builder in John S Macdonald Builders Ltd, because we felt that John was genuine in the way he cared for his customers and that he was determined to produce a high quality product. We also felt that he was committed to the Masonry concept and was willing and prepared to build what we wanted.</p>
                        <p>The fact that our home was going to be a show home for John's company before we took possession made the task challenging, as we wanted to push the boundaries a bit with our fixtures, fittings and colour schemes. The sourcing of materials and blending of ingredients was done to a very high standard by John, resulting in a home that stood out from the crowd and challenged the thinking of show home visitors. Most importantly though, the most pleasing aspect was that people were impressed with the quality of the workmanship.</p>

                        <p>John has a great team of staff and subbies who are all committed to supporting him. This meant that attention to detail was excellent. John also has an innovative approach to problem solving, particularly with regards to the more intricate construction details. This made the whole project run smoothly.</p>

                        <p>We are pleased that we chose to build with John and are absolutely thrilled with the end result. We have no hesitation in recommending him to others. </p>

                        <p>Regards,</p>
                        <p><span class="review-author">Andrew Porter</span></p>
                    </blockquote>

                    <p>Did we do a great job on your home or project? You can send us your feedback using the form on our <a href="contact">contact page</a>, via <a href="mailto:john@jsmb.co.nz">email</a>, or leave a review on our <a href="https://plus.google.com/+JohnSMacdonaldBuildersLtdHamilton/about?hl=en" target="_blank">Google+ Page</a>.
                        <br/> We would love to hear from you.</p>
                </div>
            </div>
            <!-- /.row -->
    
        <?php include_once 'footer.php'; ?>