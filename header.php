<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="wot-verification" content="b49033317ac1e65b64e2" />
    <meta name="description" content="<?php echo $description ?>">

    <title><?php echo $title ?></title>

    <link href="_img/favicon.PNG" rel="icon">

    <!-- Bootstrap Core CSS -->
    <link href="_css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Theme CSS -->
    <link href="_css/bootstrap-theme.min.css" rel="stylesheet">
    
    <!-- Lightbox CSS -->
    <link rel="stylesheet" href="_css/lightbox.min.css" />

    <!-- User Styles -->
    <link href="_css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php include_once("analyticstracking.php") ?>
</head>

<body>
    <header role="banner">
        <!-- Navigation -->

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">

            <div class="container-fluid contact-bar">
                <div class="container">
                    <span class="contact-us">
            <b>Ph: </b><a href="tel:+6478558855">07 855 8855</a> | 
                <a href="mailto:john@jsmb.co.nz" target="_top"><b>Email Us </b></a>
                         </span>
                </div>
            </div>

            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="_img/jsmb-logo-small.png" alt="John S Macdonald Builders"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->

                <div class="navbar-menu">
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="/">Home</a>
                            </li>
                            <li>
                                <a href="about">About</a>
                            </li>
                            <li>
                                <a href="gallery">Gallery</a>
                            </li>
                            <li>
                                <a href="testimonials">Testimonials</a>
                            </li>
                            <li>
                                <a href="contact">Contact</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.navbar-menu -->
            </div>
            <!-- /.container -->
        </nav>
    </header>
