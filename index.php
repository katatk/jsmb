<?php

$title = "Home | John S Macdonald Builders NZ";
$description = "With over 20 years building experience, JSMB has built some of the most exquisite homes in the Waikato. Contact us to get started on your dream home.";

include_once 'header.php';

?>

    <!-- Page Content -->

    <div class="container main">

        <!-- Carousel image slider -->

        <div id="carousel-example-generic" class="carousel fade margin-bottom-30 padding-0" data-ride="carousel" data-interval="6000" data-pause="false">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">

                <div class="item active">
                    <a href="gallery">
                        <img src="_photos/house-of-year-2.jpg" alt="House of the Year 2016" class="feature-img">
                    </a>

                </div>
                <div class="item">
                    <a href="gallery">
                        <img src="_photos/house-of-year-1.jpg" alt="House of the Year 2016" class="feature-img">
                    </a>
                </div>
            </div>

        </div>

        <!-- Title -->
        <div class="row text-center margin-bottom-30">
            <div class="col-lg-12" role="main">
                <h1>John S Macdonald Builders is a name synonymous with <em>quality</em>.</h1>


                <blockquote>
                    <p>With over 25 years in the building industry my passion is working together with my clients to bring their ideas to life. One of my greatest strengths is the ability to actually <em>listen</em> to my customers, and then using my extensive knowledge and the highly experienced team working alongside me, we aim to exceed your expectations. I hope you enjoy this portfolio of these stunning homes as much as I enjoyed working with my clients to create them.</p>
                </blockquote>
                <img src="_img/john-signature.png" alt="John S Macdonald" class="signature">
                <p>&nbsp;</p>
            </div>


            <div class="col-lg-12">
                <div class="image-container">
                    <img src="_photos/gallery-preview.jpg" class="gallery-preview-img" alt="View Gallery">
                    <a href="gallery" class="btn btn-primary" onhover()="hoverFunction()">View Gallery</a>

                </div>
            </div>
        </div>
        <!-- /.row -->


        <?php include_once 'footer.php'; ?>
