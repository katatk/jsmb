<?php

$title = "Gallery | John S Macdonald Builders NZ";
$description = "View our gallery of stunning houses, commercial and community buildings. Including masonry, traditional and contemporary-style homes and alterations.";

include_once 'header.php';

?>

          <div class="container main">
        <div class="row">
            <div class="col-lg-12" role="main">
                    <h1>Gallery</h1>
                    <p>The team at JSMB have built a range of award-winning masonry, contemporary and traditional-style homes throughout the Waikato. But we don't just specialise in houses, we've also built commercial and community properties such as the <a href="http://thelink.org.nz/" target="_blank">St Andrew's Community Centre</a>.
                    </p>
                    <p>Select a thumbnail to view more photos of each project.</p>
                    <h2>Homes</h2>
                    <ul class="gallery clearfix">
                        
                          <li>
                    <a href="_photos/pencarrow-2/pencarrow-1-lg.jpg" data-lightbox="pencarrow-2">
                        <img src="_img/2016-local.png" class="medals medalone" alt="Master Builder Award">
                        <img src="_img/2016-supreme.png" class="medals medaltwo" alt="Master Builder Award">
                        <img src="_photos/pencarrow-2/pencarrow-1-thumbnail-sm.jpg" data-1x="_photos/pencarrow-2/pencarrow-1-thumbnail-md.jpg" data-2x="_photos/pencarrow-2/pencarrow-1-thumbnail-lg.jpg" class="hisrc gallery-images" alt="Traditional Home, Tamahere" />
                    </a>
                    <a href="_photos/pencarrow-2/pencarrow-2-lg.jpg" data-lightbox="pencarrow-2"></a>
                    <a href="_photos/pencarrow-2/pencarrow-3-lg.jpg" data-lightbox="pencarrow-2"></a>
                    <a href="_photos/pencarrow-2/pencarrow-4-lg.jpg" data-lightbox="pencarrow-2"></a>
                    <a href="_photos/pencarrow-2/pencarrow-5-lg.jpg" data-lightbox="pencarrow-2"></a>
                </li>

                
                  <li>
                    <a href="_photos/lake-domain/lake-domain-1-lg.jpg" data-lightbox="lake-domain">
                        <img src="_img/2016-local.png" class="medals medalone" alt="Master Builder Award">
                        <img src="_img/2016-gold.png" class="medals medaltwo" alt="Master Builder Award">
                        <img src="_photos/lake-domain/lake-domain-1-thumbnail-sm.jpg" data-1x="_photos/lake-domain/lake-domain-1-thumbnail-md.jpg" data-2x="_photos/lake-domain/lake-domain-1-thumbnail-lg.jpg" class="hisrc gallery-images" alt="Contemporary Home, Hamilton" />
                    </a>
                    <a href="_photos/lake-domain/lake-domain-2-lg.jpg" data-lightbox="lake-domain"></a>
                   <a href="_photos/lake-domain/lake-domain-3-lg.jpg" data-lightbox="lake-domain"></a>
                    <a href="_photos/lake-domain/lake-domain-4-lg.jpg" data-lightbox="lake-domain"></a>
                   <a href="_photos/lake-domain/lake-domain-5-lg.jpg" data-lightbox="lake-domain"></a>
                   
                </li>
                        
                        <li>
                            <a href="_photos/davison/davison-1-lg.jpg" data-lightbox="davison">
                                <img src="_img/2009-local.png" class="medals medalone" alt="Master Builder Award">
                                <img src="_img/2009-gold.png" class="medals medaltwo" alt="Master Builder Award">
                                <img src="_img/2009-gold-reserve.png" class="medals medalthree" alt="Master Builder Award">
                                <img src="_photos/davison/davison-1-thumbnail-sm.jpg" data-1x="_photos/davison/davison-1-thumbnail-md.jpg" data-2x="_photos/davison/davison-1-thumbnail-lg.jpg" class="hisrc gallery-images" alt="Masonry Villa Home, Tamahere" />
                            </a>
                            <a href="_photos/davison/davison-2-lg.jpg" data-lightbox="davison"></a>
                            <a href="_photos/davison/davison-4-lg.jpg" data-lightbox="davison"></a>
                            <a href="_photos/davison/davison-5-lg.jpg" data-lightbox="davison"></a>
                            <a href="_photos/davison/davison-6-lg.jpg" data-lightbox="davison"></a>
                            <a href="_photos/davison/davison-7-lg.jpg" data-lightbox="davison"></a>
                        </li>

                        <li>
                            <a href="_photos/newells-road/newells-1-lg.jpg" data-lightbox="newells">
                                <img src="_photos/newells-road/newells-1-thumbnail-sm.jpg" data-1x="_photos/newells-road/newells-1-thumbnail-md.jpg" data-2x="_photos/newells-road/newells-1-thumbnail-lg.jpg" class="hisrc gallery-images" alt="Masonry Villa Home">
                            </a>
                            <a href="_photos/newells-road/newells-3-lg.jpg" data-lightbox="newells"></a>
                            <a href="_photos/newells-road/newells-5-lg.jpg" data-lightbox="newells"></a>
                            <a href="_photos/newells-road/newells-6-lg.jpg" data-lightbox="newells"></a>
                            <a href="_photos/newells-road/newells-7-lg.jpg" data-lightbox="newells"></a>
                        </li>

                        <li>
                            <a href="_photos/san-clemento-way/san-clemento-1-lg.jpg" data-lightbox="san-clemento">
                                <img src="_photos/san-clemento-way/san-clemento-1-thumbnail-sm.jpg" data-1x="_photos/san-clemento-way/san-clemento-1-thumbnail-md.jpg" data-2x="_photos/san-clemento-way/san-clemento-1-thumbnail-lg.jpg" class="hisrc gallery-images" alt="Masonry Villa Home">
                            </a>
                            <a href="_photos/san-clemento-way/san-clemento-2-lg.jpg" data-lightbox="san-clemento"></a>
                            <a href="_photos/san-clemento-way/san-clemento-3-lg.jpg" data-lightbox="san-clemento"></a>
                            <a href="_photos/san-clemento-way/san-clemento-4-lg.jpg" data-lightbox="san-clemento"></a>
                        </li>

                        <li>
                            <a href="_photos/harrowfield-drive/harrow-1-lg.jpg" data-lightbox="harrowfield-drive">

                                <img src="_img/2006-local.png" class="medals medalone" alt="Master Builder Award">

                                <img src="_img/2006-gold-reserve.png" class="medals medaltwo" alt="Master Builder Award">

                                <img src="_img/2006-gold.png" class="medals medalthree" alt="Master Builder Award">

                                <img src="_photos/harrowfield-drive/harrow-1-thumbnail-sm.jpg" data-1x="_photos/harrowfield-drive/harrow-1-thumbnail-md.jpg" data-2x="_photos/harrowfield-drive/harrow-1-thumbnail-lg.jpg" class="hisrc gallery-images" alt="Masonry Villa Home">
                            </a>
                            <a href="_photos/harrowfield-drive/harrow-2-lg.jpg" data-lightbox="harrowfield-drive"></a>
                            <a href="_photos/harrowfield-drive/harrow-3-lg.jpg" data-lightbox="harrowfield-drive"></a>
                            <a href="_photos/harrowfield-drive/harrow-4-lg.jpg" data-lightbox="harrowfield-drive"></a>
                            <a href="_photos/harrowfield-drive/harrow-5-lg.jpg" data-lightbox="harrowfield-drive"></a>
                        </li>


                        <li>
                            <a href="_photos/river-road/river-road-1-lg.jpg" data-lightbox="river-road">
                                <img src="_img/2008-gold-reserve.png" class="medals medalone" alt="Master Builder Award">

                                <img src="_img/2008-gold.png" class="medals medaltwo" alt="Master Builder Award">

                                <img src="_photos/river-road/river-road-1-thumbnail-sm.jpg" data-1x="_photos/river-road/river-road-1-thumbnail-md.jpg" data-2x="_photos/river-road/river-road-1-thumbnail-lg.jpg" class="hisrc gallery-images" alt="Traditional Home">
                            </a>
                            <a href="_photos/river-road/river-road-2-lg.jpg" data-lightbox="river-road"></a>
                            <a href="_photos/river-road/river-road-3-lg.jpg" data-lightbox="river-road"></a>
                            <a href="_photos/river-road/river-road-4-lg.jpg" data-lightbox="river-road"></a>
                            <a href="_photos/river-road/river-road-5-lg.jpg" data-lightbox="river-road"></a>
                            <a href="_photos/river-road/river-road-7-lg.jpg" data-lightbox="river-road"></a>
                        </li>

                        <li>
                            <a href="_photos/pickering/pickering-1-lg.jpg" data-lightbox="pickering-road">
                                <img src="_photos/pickering/pickering-1-thumbnail-sm.jpg" data-1x="_photos/pickering/pickering-1-thumbnail-md.jpg" data-2x="_photos/pickering/pickering-1-thumbnail-lg.jpg" class="hisrc gallery-images" alt="Traditional Home">
                            </a>
                            <a href="_photos/pickering/pickering-2-lg.jpg" data-lightbox="pickering-road"></a>
                            <a href="_photos/pickering/pickering-3-lg.jpg" data-lightbox="pickering-road"></a>
                            <a href="_photos/pickering/pickering-4-lg.jpg" data-lightbox="pickering-road"></a>
                            <a href="_photos/pickering/pickering-6-lg.jpg" data-lightbox="pickering-road"></a>
                            <a href="_photos/pickering/pickering-7-lg.jpg" data-lightbox="pickering-road"></a>
                        </li>

                        <li>
                            <a href="_photos/chartwell/chartwell-1-lg.jpg" data-lightbox="chartwell" data-title="Chartwell">
                                <img src="_photos/chartwell/chartwell-1-thumbnail-sm.jpg" data-1x="_photos/chartwell/chartwell-1-thumbnail-md.jpg" data-2x="_photos/chartwell/chartwell-1-thumbnail-lg.jpg" class="hisrc gallery-images" alt="Traditional Home">
                            </a>
                            <a href="_photos/chartwell/chartwell-2-lg.jpg" data-lightbox="chartwell"></a>
                            <a href="_photos/chartwell/chartwell-3-lg.jpg" data-lightbox="chartwell"></a>
                            <a href="_photos/chartwell/chartwell-4-lg.jpg" data-lightbox="chartwell"></a>
                            <a href="_photos/chartwell/chartwell-5-lg.jpg" data-lightbox="chartwell"></a>
                        </li>
                        
                         <li>
                            <a href="_photos/pencarrow/pencarrow-1-lg.jpg" data-lightbox="pencarrow" data-title="Pencarrow">
                                <img src="_photos/pencarrow/pencarrow-1-th-sm.jpg" data-1x="_photos/pencarrow/pencarrow-1-th-md.jpg" data-2x="_photos/pencarrow/pencarrow-1-th-lg.jpg" class="hisrc gallery-images" alt="Masonry Home">
                            </a>
                            <a href="_photos/pencarrow/pencarrow-8-lg.jpg" data-lightbox="pencarrow"></a> 
                            <a href="_photos/pencarrow/pencarrow-2-lg.jpg" data-lightbox="pencarrow"></a>
                            <a href="_photos/pencarrow/pencarrow-3-lg.jpg" data-lightbox="pencarrow"></a>
                            <a href="_photos/pencarrow/pencarrow-4-lg.jpg" data-lightbox="pencarrow"></a>
                            <a href="_photos/pencarrow/pencarrow-5-lg.jpg" data-lightbox="pencarrow"></a>
                            <a href="_photos/pencarrow/pencarrow-6-lg.jpg" data-lightbox="pencarrow"></a>
                            <a href="_photos/pencarrow/pencarrow-9-lg.jpg" data-lightbox="pencarrow"></a> 
                           
                        </li>
                    </ul>

                    <h2>Commercial &amp; Community Buildings</h2>
                    <ul class="gallery clearfix">
                        <li>
                            <a href="_photos/eventide/eventide-1-lg.jpg" data-lightbox="eventide" data-title="Eventide Apartments, Tamahere">

                                <img src="_img/2006-local.png" class="medals medalone" alt="Master Builder Award">

                                <img src="_img/2006-gold-reserve.png" class="medals medaltwo" alt="Master Builder Award">

                                <img src="_img/2006-gold.png" class="medals medalthree" alt="Master Builder Award">

                                <img src="_photos/eventide/eventide-1-thumbnail-sm.jpg" data-1x="_photos/eventide/eventide-1-thumbnail-md.jpg" data-2x="_photos/eventide/eventide-1-thumbnail-lg.jpg" class="hisrc gallery-images" alt="Eventide Apartments, Tamahere">
                            </a>

                            <a href="_photos/eventide/eventide-2-lg.jpg" data-lightbox="eventide" data-title="Eventide Apartments, Tamahere"></a>
                            <a href="_photos/eventide/eventide-3-lg.jpg" data-lightbox="eventide" data-title="Eventide Apartments, Tamahere"></a>
                            <a href="_photos/eventide/eventide-4-lg.jpg" data-lightbox="eventide" data-title="Eventide Apartments, Tamahere"></a>
                            <a href="_photos/eventide/eventide-6-lg.jpg" data-lightbox="eventide" data-title="Eventide Apartments, Tamahere"></a>
                            <a href="_photos/eventide/eventide-5-lg.jpg" data-lightbox="eventide" data-title="Eventide Apartments, Tamahere"></a>
                        </li>


                        <li>
                            <a href="_photos/st-andrews/st-andrews-1-lg.jpg" data-lightbox="st-andrews" data-title="St Andrews Community Centre, Claudelands">
                                <img src="_photos/st-andrews/st-andrews-1-thumbnail-sm.jpg" data-1x="_photos/st-andrews/st-andrews-1-thumbnail-md.jpg" data-2x="_photos/st-andrews/st-andrews-1-thumbnail-lg.jpg" class="hisrc gallery-images" alt="St Andrews Community Centre, Claudelands">
                            </a>
                            <a href="_photos/st-andrews/st-andrews-2-lg.jpg" data-lightbox="st-andrews" data-title="St Andrews Community Centre, Claudelands"></a>
                            <a href="_photos/st-andrews/st-andrews-3-lg.jpg" data-lightbox="st-andrews" data-title="St Andrews Community Centre, Claudelands"></a>
                            <a href="_photos/st-andrews/st-andrews-4-lg.jpg" data-lightbox="st-andrews" data-title="St Andrews Community Centre, Claudelands"></a>
                            <a href="_photos/st-andrews/st-andrews-5-lg.jpg" data-lightbox="st-andrews" data-title="St Andrews Community Centre, Claudelands"></a>
                        </li>

                        <li>
                            <a href="_photos/spartan/spartan-1-lg.jpg" data-lightbox="spartan" data-title="Spartan Construction, Hillcrest">
                                <img src="_photos/spartan/spartan-1-thumbnail-sm.jpg" data-1x="_photos/spartan/spartan-1-thumbnail-md.jpg" data-2x="_photos/spartan/spartan-1-thumbnail-lg.jpg" class="hisrc gallery-images" alt="Spartan Construction, Hillcrest">
                            </a>
                            <a href="_photos/spartan/spartan-4-lg.jpg" data-lightbox="spartan" data-title="Spartan Construction, Hillcrest"></a>
                            <a href="_photos/spartan/spartan-3-lg.jpg" data-lightbox="spartan" data-title="Spartan Construction, Hillcrest"></a>
                            <a href="_photos/spartan/spartan-2-lg.jpg" data-lightbox="spartan" data-title="Spartan Construction, Hillcrest"></a>
                            <a href="_photos/spartan/spartan-5-lg.jpg" data-lightbox="spartan" data-title="Spartan Construction, Hillcrest"></a>
                        </li>


                        <li>
                            <a href="_photos/davis-corner/davis-corner-1-lg.jpg" data-lightbox="davies-corner" data-title="Davies Corner, Chartwell">
                                <img src="_photos/davis-corner/davis-corner-1-thumbnail-sm.jpg" data-1x="_photos/davis-corner/davis-corner-1-thumbnail-md.jpg" data-2x="_photos/davis-corner/davis-corner-1-thumbnail-lg.jpg" class="hisrc gallery-images" alt="Davies Corner, Chartwell">
                            </a>
                            <a href="_photos/davis-corner/davis-corner-2-lg.jpg" data-lightbox="davies-corner" data-title="Davies Corner, Chartwell"></a>
                            <a href="_photos/davis-corner/davis-corner-4-lg.jpg" data-lightbox="davies-corner" data-title="Davies Corner, Chartwell"></a>
                        </li>
                    </ul>

            </div>
  </div>
        <!-- /.row -->

        <?php include_once 'footer.php'; ?>
