<hr>
<footer>
    <div class="row">
        <div class="col-lg-12 copyright">
            <p>&copy; John S Macdonald Builders
                <?php echo date("Y") ?>, all rights reserved</p>

            <a href="http://www.masterbuilder.org.nz" target="_blank" /><img class="footer-logo" src="_img/MB-Logo-Small.png" alt="Registered Master Builder"></a>
        </div>
    </div>
</footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>

<!-- Bootstrap Core JavaScript -->
<script src="_js/bootstrap.min.js"></script>

<!-- Lightbox -->
<script src="_js/lightbox.min.js"></script>
<script src="_js/hisrc.min.js"></script>

<!-- Custom JavaScript -->
<script src="_js/custom.js"></script>
<script src="_js/gallery.min.js"></script>

<!-- Parsley -->
<script src="_js/parsley.min.js"></script>

<!-- Google recaptcha script -->
<script src='https://www.google.com/recaptcha/api.js' async defer></script>

</body>

</html>